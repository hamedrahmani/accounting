package com.bankdemo.accounting.dao;

import com.bankdemo.accounting.entity.TransactionLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionLogRepository extends JpaRepository<TransactionLog,Integer> {
}
