package com.bankdemo.accounting.dao;

import com.bankdemo.accounting.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account,Integer> {
}
