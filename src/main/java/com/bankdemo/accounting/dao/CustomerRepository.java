package com.bankdemo.accounting.dao;

import com.bankdemo.accounting.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,Integer> {
}
