package com.bankdemo.accounting.model;

import java.math.BigDecimal;

public class TransferDTO {
    public int sourceAccountId;
    public int targetAccountId;
    public BigDecimal amount;
    public CurrencyType currencyType;

    public TransferDTO(int sourceAccountId, int targetAccountId, BigDecimal amount, CurrencyType currencyType) {
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.amount = amount;
        this.currencyType = currencyType;
    }

    public TransferDTO() {
    }
}
