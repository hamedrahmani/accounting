package com.bankdemo.accounting.model;

import java.math.BigDecimal;

public class ChangeBalanceDTO {
    public ChangeBalanceDTO(int accountId, BigDecimal amount, CurrencyType currencyType) {
        this.accountId = accountId;
        this.amount = amount;
        this.currencyType =  currencyType;
    }

    public ChangeBalanceDTO() {

    }

    public int accountId;
    public BigDecimal amount;
    public CurrencyType currencyType;
}
