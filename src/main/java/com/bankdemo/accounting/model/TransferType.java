package com.bankdemo.accounting.model;

public enum TransferType {
    Withdraw(1),
    Deposit(2),
    TransferMoney(3);

    private final int transferType;

    TransferType(int i) {
        this.transferType = i;
    }

    public int getTransferType() {
        return transferType;
    }
}
