package com.bankdemo.accounting.model;

public enum CurrencyType {
    Rial(1),
    Dollor(2),
    ;

    private final int currencyType;

    CurrencyType(int i) {
        this.currencyType = i;
    }

    public int getCurrencyType() {
        return currencyType;
    }
}
