package com.bankdemo.accounting.model;

import java.math.BigDecimal;

public class CustomerDTO {
    public String nationalCode;
    public String firstName;
    public String lastName;
    public String phoneNumber;

}
