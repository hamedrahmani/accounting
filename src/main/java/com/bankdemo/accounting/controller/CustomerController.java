package com.bankdemo.accounting.controller;

import com.bankdemo.accounting.entity.Account;
import com.bankdemo.accounting.entity.Customer;
import com.bankdemo.accounting.model.CustomerDTO;
import com.bankdemo.accounting.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping()
    public List<Customer> findAll() {
        return customerService.findAll();
    }

    @GetMapping("/{customerId}")
    public Customer findById(@PathVariable int customerId) {
        Customer c = customerService.findById(customerId);
        Set<Account> a = c.getAccounts();
        return c;
    }

    @PostMapping()
    public Customer addCustomer(@Validated @RequestBody Customer customer) {
        customer.setId(0);
        customerService.save(customer);
        return customer;
    }

    @PutMapping()
    public Customer putCustomer(@RequestBody Customer customer) {
        customerService.save(customer);
        return customer;
    }
}
