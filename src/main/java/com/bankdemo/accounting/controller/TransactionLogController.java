package com.bankdemo.accounting.controller;

import com.bankdemo.accounting.entity.Account;
import com.bankdemo.accounting.entity.TransactionLog;
import com.bankdemo.accounting.model.ChangeBalanceDTO;
import com.bankdemo.accounting.model.TransferDTO;
import com.bankdemo.accounting.service.account.AccountService;
import com.bankdemo.accounting.service.transactionlog.TransactionLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/transactionlog")
public class TransactionLogController {

    @Autowired
    private TransactionLogService transactionLogService;

    @GetMapping()
    public List<TransactionLog> findAll() {
        return transactionLogService.findAll();
    }
    @GetMapping("/{sourceId}")
    public List<TransactionLog> findBySourceId(@PathVariable int sourceId) {

        return transactionLogService.findBySourceId(sourceId);
    }
    @GetMapping("/targetId")
    public List<TransactionLog> findByTargetId(@PathVariable int targetId) {
        return transactionLogService.findByTargetId(targetId);
    }

}
