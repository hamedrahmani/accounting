package com.bankdemo.accounting.controller;

import com.bankdemo.accounting.entity.Account;
import com.bankdemo.accounting.model.ChangeBalanceDTO;
import com.bankdemo.accounting.model.TransferDTO;
import com.bankdemo.accounting.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping
    public List<Account> findAll() {
//         CurrencyType value = CurrencyType.valueOf("Rial");
//        int val = CurrencyType.Rial.getCurrencyType();
        return accountService.findAll();
    }

    @PostMapping()
    public Account addAccount(@RequestBody Account account) {
        account.setId(0);
        accountService.save(account);
        return account;
    }
    @PostMapping("/deposit")
    public Account increaseAmount(@RequestBody ChangeBalanceDTO changeBalanceDto)  {
        Account account = accountService.deposit(changeBalanceDto, true);
        return account;
    }
    @PostMapping("/withdraw")
    public Account withdraw(@RequestBody ChangeBalanceDTO changeBalanceDto)  {
        Account account = accountService.withdraw(changeBalanceDto, true);
        return account;
    }
    @PostMapping("/transfer")
    public Account transferMoney(@RequestBody TransferDTO transferDTO)  {
        Account account = accountService.transferMoney(transferDTO);
        return account;
    }
}
