package com.bankdemo.accounting.entity;


import com.bankdemo.accounting.model.CurrencyType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

//    @JsonProperty("customer_id")
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)//(optional=false)
    @JoinColumn(name = "customer_id")
    @JsonIgnore
    private Customer customer;

    @Column(unique = true)
    private String accountNumber;
    @Column(nullable = false)
    private BigDecimal balance;
    @Column(nullable = false)
    private CurrencyType currencyType;

    public Account(Customer customer, String accountNumber, BigDecimal balance, CurrencyType currencyType) {
        this.customer = customer;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.currencyType = currencyType;
    }

    public Account() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonIgnore
    public Customer getCustomer() {
        return customer;
    }

//    public int getCustomer_id() {
//        return customer.getId();
//    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", customer=" + customer +
                ", accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                ", currencyType=" + currencyType +
                '}';
    }
}
