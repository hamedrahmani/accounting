package com.bankdemo.accounting.entity;


import com.bankdemo.accounting.model.CurrencyType;
import com.bankdemo.accounting.model.TransferType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class TransactionLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable=true)
    private int sourceAccountId;
    @Column(nullable=true)
    private int targetAccountId;
    @Column(nullable = false)
    private BigDecimal balance;
    @Column(nullable = false)
    private CurrencyType currencyType;
    @Column(nullable = false)
    private TransferType transferType;


    public TransactionLog(){

    }

    public TransactionLog(Integer sourceAccountId, Integer targetAccountId, BigDecimal balance,CurrencyType currencyType, TransferType transferType) {
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.balance = balance;
        this.transferType = transferType;
        this.currencyType = currencyType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSourceAccountId() {
        return sourceAccountId;
    }

    public void setSourceAccountId(int sourceAccountId) {
        this.sourceAccountId = sourceAccountId;
    }

    public int getTargetAccountId() {
        return targetAccountId;
    }

    public void setTargetAccountId(int targetAccountId) {
        this.targetAccountId = targetAccountId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public TransferType getTransferType() {
        return transferType;
    }

    public void setTransferType(TransferType transferType) {
        this.transferType = transferType;
    }


    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    @Override
    public String toString() {
        return "TransactionLog{" +
                "id=" + id +
                ", sourceAccountId=" + sourceAccountId +
                ", targetAccountId=" + targetAccountId +
                ", balance=" + balance +
                ", transferType=" + transferType +
                ", currencyType=" + currencyType +
                '}';
    }
}
