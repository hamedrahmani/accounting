package com.bankdemo.accounting.service.account;

import com.bankdemo.accounting.common.BalanceException;
import com.bankdemo.accounting.common.NotFoundException;
import com.bankdemo.accounting.entity.Account;
import com.bankdemo.accounting.dao.AccountRepository;
import com.bankdemo.accounting.entity.TransactionLog;
import com.bankdemo.accounting.model.ChangeBalanceDTO;
import com.bankdemo.accounting.model.TransferDTO;
import com.bankdemo.accounting.model.TransferType;
import com.bankdemo.accounting.service.exchange.ExchangeService;
import com.bankdemo.accounting.service.transactionlog.TransactionLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class AccountServiceImp implements AccountService {
    @Autowired
    private AccountRepository repostiroy;
    @Autowired
    private ExchangeService exchangeService;
    @Autowired
    private TransactionLogService transactionLogService;

    @Override
    public void save(Account account) {
        repostiroy.save(account);
    }

    @Override
    public List<Account> findAll() {
        return repostiroy.findAll();
    }

    @Override
    public Account deposit(ChangeBalanceDTO changeBalanceDto, boolean autoSave) {
        Account account = getAccountById(changeBalanceDto.accountId);
        exchange(changeBalanceDto, account);
        account.setBalance(account.getBalance().add(changeBalanceDto.amount));

        if (autoSave) {
            repostiroy.save(account);
            TransactionLog transactionLog =
                    new TransactionLog(changeBalanceDto.accountId, changeBalanceDto.accountId, changeBalanceDto.amount, changeBalanceDto.currencyType, TransferType.Deposit);
            saveTransactionLog(transactionLog);
        }
        return account;
    }

    @Override
//    @Async
    public Account withdraw(ChangeBalanceDTO changeBalanceDto, boolean autoSave) {
        //TODO
        Account account = getAccountById(changeBalanceDto.accountId);

        exchange(changeBalanceDto, account);

        if (checkAccountBalance(account, changeBalanceDto.amount)) {
            account.setBalance(account.getBalance().subtract(changeBalanceDto.amount));
            if (autoSave) {
                repostiroy.save(account);
                TransactionLog transactionLog =
                        new TransactionLog(changeBalanceDto.accountId, changeBalanceDto.accountId, changeBalanceDto.amount, changeBalanceDto.currencyType, TransferType.Withdraw);

                saveTransactionLog(transactionLog);
            }
        } else throw new BalanceException("Balance is less than withdraw amount");
        return account;
    }

    private void exchange(ChangeBalanceDTO changeBalanceDto, Account account) {
        if (changeBalanceDto.currencyType != account.getCurrencyType()) {
            changeBalanceDto.amount = exchangeService.ExchangeMoney(changeBalanceDto.amount, changeBalanceDto.currencyType);
        }
    }

    @Override
    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    public Account transferMoney(TransferDTO transferDto) {


        ChangeBalanceDTO withdrawChangeBalance = new ChangeBalanceDTO(transferDto.sourceAccountId, transferDto.amount, transferDto.currencyType);
        Account sourceAccount = withdraw(withdrawChangeBalance, false);

        ChangeBalanceDTO increaseChangeBalance = new ChangeBalanceDTO(transferDto.targetAccountId, transferDto.amount, transferDto.currencyType);
        Account targetAccount = deposit(increaseChangeBalance, false);

        List<Account> accounts = Arrays.asList(sourceAccount, targetAccount);
        repostiroy.saveAll(accounts);
        TransactionLog transactionLog = new TransactionLog(transferDto.sourceAccountId, transferDto.targetAccountId, transferDto.amount, transferDto.currencyType, TransferType.TransferMoney);

        saveTransactionLog(transactionLog);

        return null;
    }

    private boolean checkAccountBalance(Account account, BigDecimal amount) {
        if (account.getBalance().compareTo(amount) == -1) return false;
        return true;
    }

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    private Account getAccountById(int accountId) {
        return repostiroy.findById(accountId).orElseThrow(() -> new NotFoundException("Account not found"));

    }

    private void saveTransactionLog(TransactionLog transactionLog) {
        transactionLogService.save(transactionLog);
    }
}
