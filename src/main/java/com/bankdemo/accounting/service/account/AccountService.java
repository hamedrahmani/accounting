package com.bankdemo.accounting.service.account;

import com.bankdemo.accounting.entity.Account;
import com.bankdemo.accounting.model.ChangeBalanceDTO;
import com.bankdemo.accounting.model.TransferDTO;

import java.math.BigDecimal;
import java.util.List;


public interface AccountService {
    void save(Account account);

    List<Account> findAll();

    Account deposit(ChangeBalanceDTO changeBalanceDTO, boolean autoSave);

    Account withdraw(ChangeBalanceDTO changeBalanceDTO, boolean autoSave);

    Account transferMoney(TransferDTO transferDTO);
}
