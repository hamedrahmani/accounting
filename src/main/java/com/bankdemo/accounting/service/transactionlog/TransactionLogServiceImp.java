package com.bankdemo.accounting.service.transactionlog;

import com.bankdemo.accounting.dao.TransactionLogRepository;
import com.bankdemo.accounting.entity.TransactionLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionLogServiceImp implements TransactionLogService {
    @Autowired
    private TransactionLogRepository repository;

    @Override
    public void save(TransactionLog transactionLog) {
        repository.save(transactionLog);
    }

    @Override
    public List<TransactionLog> findAll() {
        return repository.findAll();
    }

    @Override
    public List<TransactionLog> findBySourceId(int sourceId) {
        return null;
    }

    @Override
    public List<TransactionLog> findByTargetId(int targetId) {
        return null;
    }
}
