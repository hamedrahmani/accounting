package com.bankdemo.accounting.service.transactionlog;

import com.bankdemo.accounting.entity.TransactionLog;

import java.util.List;

public interface TransactionLogService {
    void save(TransactionLog transactionLog);

    List<TransactionLog> findAll();

    List<TransactionLog> findBySourceId(int sourceId);

    List<TransactionLog> findByTargetId(int targetId);
}
