package com.bankdemo.accounting.service.customer;

import com.bankdemo.accounting.common.NotFoundException;
import com.bankdemo.accounting.dao.CustomerRepository;
import com.bankdemo.accounting.entity.Account;
import com.bankdemo.accounting.entity.Customer;
import com.bankdemo.accounting.model.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class CustomerServiceImp implements CustomerService {
    @Autowired
    private  CustomerRepository repostiroy;
    @Override
    public void save(Customer customer) {
        Customer ctm = new Customer();

        Set<Account> accounts = new HashSet<>();

        customer.getAccounts().stream().forEach(item ->{
            Account account = new Account();
            account.setCustomer(customer);
            accounts.add(account);
        });
        ctm.setAccounts(accounts);
        repostiroy.save(customer);
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> res = repostiroy.findAll();
        return res;

    }
    @Override
    public Customer findById(int id) {
        Customer res = repostiroy.findById(id).orElseThrow(() -> new NotFoundException("Customer not Found"));
        return res;

    }

}
