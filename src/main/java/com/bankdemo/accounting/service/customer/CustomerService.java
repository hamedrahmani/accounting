package com.bankdemo.accounting.service.customer;

import com.bankdemo.accounting.entity.Customer;
import com.bankdemo.accounting.model.CustomerDTO;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


public interface CustomerService {
     void save(Customer customer);
     List<Customer> findAll();
     Customer findById(int id);
}
