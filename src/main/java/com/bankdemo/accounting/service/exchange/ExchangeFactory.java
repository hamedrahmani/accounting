package com.bankdemo.accounting.service.exchange;

import com.bankdemo.accounting.model.CurrencyType;

public class ExchangeFactory  {
    public Exchange getExchange(CurrencyType currencyType){
        switch (currencyType){
            case Rial:
                return  new ExchangeRial();
            case Dollor:
                return  new ExchangeDollar();
        }
        return null;
    }
}

//public final class ClassSingleton {
//
//    private static ClassSingleton INSTANCE;
//    private String info = "Initial info class";
//
//    private ClassSingleton() {
//    }
//
//    public static ClassSingleton getInstance() {
//        if(INSTANCE == null) {
//            INSTANCE = new ClassSingleton();
//        }
//
//        return INSTANCE;
//    }
//
//    // getters and setters
//}
