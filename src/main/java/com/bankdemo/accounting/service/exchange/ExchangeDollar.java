package com.bankdemo.accounting.service.exchange;

import java.math.BigDecimal;

public class ExchangeDollar implements Exchange {
    @Override
    public BigDecimal doExchange( BigDecimal price) {
        return price.multiply(BigDecimal.valueOf(180000));
    }
}
