package com.bankdemo.accounting.service.exchange;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ExchangeRial implements Exchange {
    @Override
    public BigDecimal doExchange( BigDecimal price) {
        return price.divide(BigDecimal.valueOf(180000),3, RoundingMode.CEILING);
    }
}
