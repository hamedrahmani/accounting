package com.bankdemo.accounting.service.exchange;

import com.bankdemo.accounting.model.CurrencyType;

import java.math.BigDecimal;

public interface ExchangeService {
    BigDecimal ExchangeMoney(BigDecimal amount, CurrencyType currencyType);
}
