package com.bankdemo.accounting.service.exchange;

import com.bankdemo.accounting.model.CurrencyType;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ExchangeServiceImp implements ExchangeService {
    @Override
    public BigDecimal ExchangeMoney(BigDecimal amount, CurrencyType currencyType) {
        ExchangeFactory exchangeFactory = new ExchangeFactory();
        Exchange exchange = exchangeFactory.getExchange(currencyType);
        return exchange.doExchange(amount);
    }
}
