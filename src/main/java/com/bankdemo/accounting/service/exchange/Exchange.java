package com.bankdemo.accounting.service.exchange;

import java.math.BigDecimal;

public interface Exchange {
     BigDecimal doExchange(BigDecimal price);
}
